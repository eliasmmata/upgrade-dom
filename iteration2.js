// Iteración #1: Interacción con el DOM
function init() {
// 2.1 Inserta dinamicamente en un html un div vacio con javascript.
var newElem = document.createElement('div');
newElem.id = 'nuevoElemento';
newElem.className = 'div Ejercicio2-1';
newElem.style = 'border: 1px solid black; width:100%; height:100px';
var body = document.querySelector('body');
body.appendChild(newElem);

// 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
var secondDiv = document.createElement('div');
secondDiv.id = 'secondDiv';
secondDiv.className = 'div Ejercicio2-2';
secondDiv.style = 'margin-top: 1em;border: 1px solid red; width:100%; height:100px';
body.appendChild(secondDiv);

var pInsideDiv = document.createElement('p');
secondDiv.appendChild(pInsideDiv);

pInsideDiv.className = 'p-Ejercicio2-2';
pInsideDiv.id = 'p-Ej-2-2';

document.getElementById("p-Ej-2-2").textContent += " Soy el div del Ej 2.2.";

// 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
var thirdDiv = document.createElement('div');
thirdDiv.id = 'thirdDiv';
thirdDiv.className = 'div Ejercicio3-3';
thirdDiv.style = 'margin-top: 1em;border: 1px solid black; width:100%; height:100px';
body.appendChild(thirdDiv);

for(var i = 0; i < 6; i++) {
    var pInLoop = document.createElement('p');
    thirdDiv.appendChild(pInLoop);
    pInLoop.className = 'pLoop3-3';
    pInLoop.style = 'width: 15%; margin: 0% 2.5%; display: inline';
    var textP = "Hello World!";
    pInLoop.append(textP);
}

// 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.
var fourDiv = document.createElement('div');
fourDiv.id = 'fourDiv';
fourDiv.className = 'div Ejercicio2-4';
fourDiv.style = 'margin-top: 1em;border: 1px solid red; width:100%; height:100px';
body.appendChild(fourDiv);

var pInsideDivFour = document.createElement('p');
fourDiv.appendChild(pInsideDivFour);
pInsideDivFour.id = 'pDinamico'
document.getElementById("pDinamico").textContent += " Soy dinámico : )";

// 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
var targetDiv = document.querySelector('h2.fn-insert-here');
var wubba = targetDiv;
wubba.innerHTML = 'Wubba Lubba dub dub Ej 2.5';

// 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.

var unordenedList = document.createElement('ul'); // no sé hacerla global dentro de la funciòn y no recuerdo si declararla fuera es peor
const createUl = () => {
    unordenedList.className = 'Clase Ul';
    unordenedList.id = 'primer-ul';
    unordenedList.style = 'margin-top: 1em;border: 1px solid red; height:100px; display: flex; justify-content: space-evenly;';
    body.appendChild(unordenedList);
}
createUl()

const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

for(var i = 0; i < apps.length; i++) {
    var $$liItem = document.createElement('li');
    unordenedList.appendChild($$liItem);
    $$liItem.className = 'liItem';
    $$liItem.style = 'color: yellow, width: 20%;';
    var nombreApp = apps[i];
    $$liItem.textContent = nombreApp;
}

// 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
const removeElements = (elms) => elms.forEach(el => el.remove());
removeElements(document.querySelectorAll(".fn-remove-me"));


// 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div.
// 	Recuerda que no solo puedes insertar elementos con .appendChild.
var $$middleText = document.createElement('p');
$$middleText.id = 'textMiddleTwoDivs';
$$middleText.style = 'border-bottom: 1px dotted blue;padding: 1em';
$$middleText.textContent += " Soy p del ejercicio 2.8 que va en medio de dos divs";
body.insertBefore($$middleText, secondDiv);


// 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here
var $$allClass = {};
$$allClass = document.getElementsByClassName("fn-insert-here");
for (var i = 0; i < $$allClass.length; i++) {
    var $$textInsideClass = document.createElement('p');
    $$allClass[i].appendChild($$textInsideClass);
    $$textInsideClass.textContent += "Voy dentro! de todos los divs pq soy el último ejercicio : )";
    $$textInsideClass.style = 'border: 1px solid #FABADA;padding: 1em 0';
    }

}
window.onload = init;

// console.log('cargo fuera init') carga primero fuera de init



