// Iteración #1: Interacción con el DOM
function init() {
    // 1.1 Usa querySelector para mostrar por consola el botón con la clase .showme
// console.log(document.querySelector('.showme'))
const $$button = document.querySelector('.showme');
console.log($$button);

// 1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado
console.log(document.querySelector('#pillado'));

// 1.3 Usa querySelector para mostrar por consola todos los p
console.log(document.querySelectorAll('p')); //
// console.log(document.getElementsByTagName('p'))

// 1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon
console.log(document.querySelectorAll('.pokemon'));  //
// console.log(document.getElementsByClassName('pokemon'))

// 1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo
// data-function="testMe".
console.log(document.querySelectorAll('[data-function="testMe"]'));

// 1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo
// data-function="testMe".
console.log(document.body.querySelector('[data-function="testMe"]:nth-of-type(3)'));

// document.querySelectorAll()  // Accede a NodeList
// document.getElementsByClassName() // Accede a HTML Collection
}

window.onload = init;